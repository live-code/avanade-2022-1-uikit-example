import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  data: any;
  data$ = new BehaviorSubject<any>(null)

  constructor() { }

  login() {
    //....
    setTimeout(() => {
      const res = { name: 'pippo', token: '123213', role: 'admin'};
      this.data = res;
      this.data$.next(res);
    }, 1000)
  }

  logout() {
    this.data = null;
    this.data$.next(null);
  }

  isLogged(): boolean {
    return !!this.data;
  }
}
