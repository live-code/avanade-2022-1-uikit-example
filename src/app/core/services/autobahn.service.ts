import { Injectable } from '@angular/core';
import { Connection, Session } from 'autobahn';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AutobahnService {
  connection!: Connection ;
  session!: Session;
  data = new BehaviorSubject<{ channel: string, data: any } | null>(null);

  init() {
    this.connection = new Connection({url: 'ws://127.0.0.1:9000/', realm: 'realm1'});

    this.connection.onopen =  (session) => {
      this.session = session;

      session.subscribe('com.myapp.hello', (args) => {
        this.data.next({
          channel: 'com.myapp.hello',
          data: args
        });
        console.log("Event:", args[0], args[1]);
      });

      session.subscribe('com.myapp.hello2', (args) => {
        this.data.next({
          channel: 'com.myapp.hello2',
          data: args
        });
        console.log("Event:", args[0], args[1]);
      });

    };

    this.connection.open();
  }

  subscribe(channel: string) {
    this.session.subscribe(channel, (args: any) => {
      this.data.next({
        channel: channel,
        data: args
      });
    });
  }

  publish(channel: string, val: any) {
    if (this.session) {
      this.session.publish(channel, val);
    }
  }
}
