export const translations: { [key: string]: any } = {
  it: {
    hello: 'ciao',
    ALL: 'tutti',
    REQUIRED: 'obbligatorio',
  },
  en: {
    hello: 'hi',
    ALL: 'all',
    REQUIRED: 'required'
  }
};
