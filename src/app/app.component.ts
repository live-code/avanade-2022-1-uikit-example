import { Component } from '@angular/core';
import { AutobahnService } from './core/services/autobahn.service';
import { AuthService } from './core/auth.service';
import { LanguageService } from './shared/pipes/language.service';
import { translations } from './translations';

@Component({
  selector: 'ava-root',
  template: `
    
    <!--The content below is only a placeholder and can be replaced.-->
    <button routerLink="login">login</button>
    <button routerLink="catalog" >catalog</button>
    
    <button routerLink="uikit-demo1" *avaIfSignin>uikit MATERIAL 1</button>
    
    <button routerLink="uikit-demo2" *avaIfRoleIs="'admin'">uikit material 2</button>
    <button routerLink="uikit-demo3">uikit 3</button>
    <button routerLink="uikit-demo4">uikit 4</button>
    <button routerLink="uikit-demo5">uikit 5</button>
    <button routerLink="uikit8pipes">pipes</button>
    <button 
      routerLink="uikit-demo6" 
      avaMyRouterLinkActive="active">uikit 6</button>
    <button
      routerLink="uikit-demo7" 
      routerLinkActive="active">uikit 7</button>
    <button
      avaIfLogged
      (click)="authService.logout()">logout</button>
    
    {{autobahnService.data | async}}

    <br>
    Current Language: {{languageService.getLanguage()}}
    Key Example {{languageService.getValue('REQUIRED')}}
    <button (click)="languageService.setLanguage('it')">IT</button>
    <button (click)="languageService.setLanguage('en')">EN</button>
    <hr>
    <router-outlet></router-outlet>
  `,
  styles: [`
    .active { background-color: orange} 
  `]
})
export class AppComponent {
  title = 'avanade2022-uikit-examples';

  constructor(
    public authService: AuthService,
    public autobahnService: AutobahnService,
    public languageService: LanguageService
  ) {
    languageService.setLanguage('it');
    // load data
    languageService.setTranslations(translations)


    /*
    autobahnService.init()
    autobahnService.publish('pippo', 123)
    autobahnService.publish('pluto', 123)

    autobahnService.data
      .subscribe(res => {
        switch(res?.channel) {
          case 'pippo':
            break;
          case 'pluto':
            break;
        }
      })
    */
  }
}
