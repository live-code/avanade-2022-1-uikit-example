import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UikitDemo1RoutingModule } from './uikit-demo1-routing.module';
import { UikitDemo1Component } from './uikit-demo1.component';
import { SharedModule } from '../../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DialogDeleteUserComponent } from './components/dialog-delete-user.component';
import { DialogAddEditUserComponent } from './components/dialog-add-edit-user.component';
import { DialogAddEditReactiveUserComponent } from './components/dialog-add-edit-reactive-user.component';


@NgModule({
  declarations: [
    UikitDemo1Component,
    DialogDeleteUserComponent,
    DialogAddEditUserComponent,
    DialogAddEditReactiveUserComponent
  ],
  imports: [
    CommonModule,
    UikitDemo1RoutingModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
  ]
})
export class UikitDemo1Module { }
