import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { User } from '../../../model/user';
import { AbstractControl, FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';

@Component({
  selector: 'ava-dialog-add-edit-reactive-user',
  template: `
    <h1 *ngIf="data; else addUserTitle">Editing {{data?.name}}</h1>
    <ng-template #addUserTitle>Add new user</ng-template>
    <hr>
    
    <form [formGroup]="form" >
      <input type="text" placeholder="name" formControlName="name">
      <input type="text" placeholder="website"  formControlName="website" >
    </form>
    
    <mat-dialog-actions align="end">
      <button mat-button mat-dialog-close>Cancel</button>
      <button 
        type="submit"
        mat-button [mat-dialog-close]="form.value" cdkFocusInitial
        [disabled]="form.invalid"
      >
        Confirm
      </button>
    </mat-dialog-actions>
  `,
  styles: [
  ]
})
export class DialogAddEditReactiveUserComponent  {
  form: FormGroup;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: User | null,
    private fb: FormBuilder
  ) {
    this.form = fb.group({
      name: ['', [Validators.required, Validators.minLength(3)] ],
      website: ['', [Validators.required, urlValidator]]
    })

    if (data) {
      this.form.patchValue(data)
    }
  }
}

const REGEX_URL = /[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)/

function urlValidator(control: FormControl): ValidationErrors | null {
  return control.value?.match(REGEX_URL) ? null : {
    website: true
  }
}
