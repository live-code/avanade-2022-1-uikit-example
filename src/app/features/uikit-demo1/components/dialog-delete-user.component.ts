import { Component, Inject, OnInit } from '@angular/core';
import { User } from '../../../model/user';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'ava-dialog-delete-user',
  template: `
    <h2 mat-dialog-title>Attenzione!</h2>
    <mat-dialog-content class="mat-typography">
      <h3>Sei sicuro di voler rimuovere {{data.username}} ?</h3>
      Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad, amet asperiores, blanditiis, deserunt enim excepturi facere libero maxime minima nulla odio officia omnis optio quidem quisquam repellat repellendus temporibus voluptates!
    </mat-dialog-content>
    
    <mat-dialog-actions align="end">
      <button mat-button mat-dialog-close>No</button>
      <button mat-button [mat-dialog-close]="true" cdkFocusInitial>Yes</button>
    </mat-dialog-actions>
  `,
})
export class DialogDeleteUserComponent {

  constructor(@Inject(MAT_DIALOG_DATA) public data: User) {
    console.log(data)
  }
}
