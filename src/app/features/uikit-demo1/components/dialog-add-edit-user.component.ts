import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { User } from '../../../model/user';

@Component({
  selector: 'ava-dialog-add-edit-user',
  template: `
    <h1 *ngIf="data; else addUserTitle">Editing {{data?.name}}</h1>
    <ng-template #addUserTitle>Add new user</ng-template>
    <hr>
    <form #f="ngForm">
      <pre>{{inputName.errors | json}}</pre>
      <input
        #input="ngModel"
        [ngModel]="data?.name"
        name="name"
        type="text"  placeholder="name" required #inputName="ngModel">
      <pre>{{inputWebsite.errors | json}}</pre>
      <input type="text" [ngModel]="data?.website" name="website" placeholder="website"
        required avaWebsiteValidator #inputWebsite="ngModel">
    </form>

    <mat-dialog-actions align="end">
      <button mat-button mat-dialog-close>Cancel</button>
      <button 
        type="submit"
        mat-button [mat-dialog-close]="f.value" cdkFocusInitial
        [disabled]="f.invalid"
      >
        Confirm
      </button>
    </mat-dialog-actions>
  `,
  styles: [
  ]
})
export class DialogAddEditUserComponent  {
  constructor(@Inject(MAT_DIALOG_DATA) public data: User | null) {
  }

}
