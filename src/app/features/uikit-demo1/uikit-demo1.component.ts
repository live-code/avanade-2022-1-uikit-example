import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../../model/user';
import { MatDialog } from '@angular/material/dialog';
import { DialogDeleteUserComponent } from './components/dialog-delete-user.component';
import { DialogAddEditUserComponent } from './components/dialog-add-edit-user.component';
import { DialogAddEditReactiveUserComponent } from './components/dialog-add-edit-reactive-user.component';

@Component({
  selector: 'ava-uikit-demo1',
  template: `
    <hr>
    <button mat-flat-button color="primary" (click)="addUserHandler()">Add User</button>

    <mat-list role="list">
      <mat-list-item *ngFor="let user of users">
        <mat-divider></mat-divider>
        
        <button mat-icon-button [matMenuTriggerFor]="menu" aria-label="Example icon-button with a menu">
          <mat-icon>more_vert</mat-icon>
        </button>
      
        <mat-menu #menu="matMenu" [hasBackdrop]="true">
          <button mat-menu-item (click)="editUserHandler(user)">
            <mat-icon>dialpad</mat-icon>
            <span>Edit User</span>
          </button>
          <button mat-menu-item  (click)="deleteUser(user)" >
            <mat-icon>delete_outline</mat-icon>
            <span>Delete User</span>
          </button>
        </mat-menu>

        {{user.name}}


      </mat-list-item>
    </mat-list>
    
  <!--  
   -->
  `,
  styles: [
  ]
})
export class UikitDemo1Component {
  users: User[] = []

  constructor(
    private http: HttpClient,
    public dialog: MatDialog
  ) {
    http.get<User[]>('https://jsonplaceholder.typicode.com/users')
      .subscribe(res => {
        this.users = res;
      })
  }

  doSomething(user: User) {
    console.log(user)
  }

  deleteUser(user: User) {
    const dialogRef = this.dialog.open(DialogDeleteUserComponent, {
      data: user
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.http.delete(`https://jsonplaceholder.typicode.com/users/${user.id}`)
          .subscribe(() => {
            const index = this.users.findIndex(u => u.id === user.id)
            this.users.splice(index, 1)
          })
      }
    });
  }

  addUserHandler() {
    // const dialogRef = this.dialog.open(DialogAddEditUserComponent, {
    const dialogRef = this.dialog.open(DialogAddEditReactiveUserComponent, {
      data: null
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('ADD', result)
      this.http.post<User>('https://jsonplaceholder.typicode.com/users', result)
        .subscribe(res => {
          this.users.push(res)
        })
    })
  }


  editUserHandler(user: User) {
    //const dialogRef = this.dialog.open(DialogAddEditUserComponent, {
    const dialogRef = this.dialog.open(DialogAddEditReactiveUserComponent, {
      data: user
    });

    dialogRef.afterClosed().subscribe(result => {
      this.http.patch<User>(`https://jsonplaceholder.typicode.com/users/${user.id}`, result)
        .subscribe(res => {
          const index = this.users.findIndex(u => u.id === user.id)
          this.users[index] = res;
        })
    })
  }
}
