import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UikitDemo1Component } from './uikit-demo1.component';

const routes: Routes = [{ path: '', component: UikitDemo1Component }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UikitDemo1RoutingModule { }
