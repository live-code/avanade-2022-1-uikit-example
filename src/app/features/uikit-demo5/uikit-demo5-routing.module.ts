import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UikitDemo5Component } from './uikit-demo5.component';

const routes: Routes = [{ path: '', component: UikitDemo5Component }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UikitDemo5RoutingModule { }
