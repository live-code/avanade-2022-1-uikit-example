import { Component, ViewChild } from '@angular/core';
import { AccordionComponent } from '../../shared/components/accordion.component';

@Component({
  selector: 'ava-uikit-demo5',
  template: `
    <ava-grid-row mq="sm">
      <ava-grid-col>1</ava-grid-col>
      <ava-grid-col>2</ava-grid-col>
      <ava-grid-col>3</ava-grid-col>
    </ava-grid-row>
    
    <div row>
      <div col></div>
    </div>
    
    <ava-accordion #acc>
      <ava-card title="Panel 1">
        <input type="text">
        <input type="text">
        <input type="text">
      </ava-card>

      <ava-card title="Panel 2">
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusamus distinctio, dolorum exercitationem incidunt magnam quod repellendus repudiandae suscipit tempore vel veritatis, vitae. Commodi eius eos repellendus unde vel vitae?
      </ava-card>

      <ava-card title="Panel 3">
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. A accusamus distinctio, dolorum exercitationem incidunt magnam quod repellendus repudiandae suscipit tempore vel veritatis, vitae. Commodi eius eos repellendus unde vel vitae?
      </ava-card>
    </ava-accordion>
    
    <button (click)="openAll(true)">open all</button>
    <button (click)="openAll(false)">close all</button>

    <div class="row">
      <div class="col">1</div>
      <div class="col">2</div>
      <div class="col">3</div>
      <div class="col">4</div>
    </div>
    
  `,
})
export class UikitDemo5Component {
  @ViewChild('acc') accordion!: AccordionComponent;

  openAll(open: boolean) {
    this.accordion.openAll(open)
  }
}
