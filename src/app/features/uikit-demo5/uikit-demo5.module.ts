import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UikitDemo5RoutingModule } from './uikit-demo5-routing.module';
import { UikitDemo5Component } from './uikit-demo5.component';
import { SharedModule } from '../../shared/shared.module';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    UikitDemo5Component
  ],
  imports: [
    CommonModule,
    UikitDemo5RoutingModule,
    SharedModule,
    FormsModule
  ]
})
export class UikitDemo5Module { }
