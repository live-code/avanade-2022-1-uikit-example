import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UitkitDemo8PipesRoutingModule } from './uitkit-demo8-pipes-routing.module';
import { UitkitDemo8PipesComponent } from './uitkit-demo8-pipes.component';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';
import { FilterByGenderPipe } from './pipes/filter-by-gender.pipe';
import { FilterByNamePipe } from './pipes/filter-by-name.pipe';
import { SortPipe } from './pipes/sort.pipe';


@NgModule({
  declarations: [
    UitkitDemo8PipesComponent,
    FilterByGenderPipe,
    FilterByNamePipe,
    SortPipe
  ],
  imports: [
    CommonModule,
    UitkitDemo8PipesRoutingModule,
    FormsModule,
    SharedModule
  ]
})
export class UitkitDemo8PipesModule { }
