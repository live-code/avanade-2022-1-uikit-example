import { Component, OnInit } from '@angular/core';

export interface User {
  id: number;
  name: string;
  age: number;
  gender: 'M' | 'F';
}

@Component({
  selector: 'ava-uitkit-demo8-pipes',
  template: `
    
   <h1> {{ 'REQUIRED' | translate}}</h1>
    
    
    <input type="text" [(ngModel)]="filterNameValue" placeholder="Name">
    <select [(ngModel)]="filterGenderValue">
      <option value="all">{{'ALL' | translate}}</option>
      <option value="M">Male</option>
      <option value="F">Female</option>
    </select>

    <select [(ngModel)]="filterASCDESC">
      <option value="ASC">ASC</option>
      <option value="DESC">DESC</option>
    </select>
    
    <li *ngFor="let user of (users | filterByGender: filterGenderValue 
                                   | filterByName: filterNameValue
                                   | sort: filterASCDESC
                                   )">
      {{user.name}} - {{user.age}} - {{user.gender}}
    </li>
  
    {{value}}
    <hr>
    {{value | number: '3.2-4'}}
    <br>
    {{value | number: '1.0-0'}}
    <hr>
    {{1646217006000 | date: 'dd/MM/yy - hh:mm' }} <br>
    {{1646217006000 | timesago }} <br>
    {{1546217006000 | timesago: false }}
    <hr>

    <h1>{{gb | memory: format : 'bytes'}}</h1>
    <h1>{{gb | memory: 'mb' : 'megabytes'}}</h1>
    <h1>{{gb | memory: 'byte' : 'bytes'}}</h1>

    <img [src]="'Trieste' | mapquest" alt="">
    <img [src]="'Palermo' | mapquest: 500 : 200" alt="">
    <hr>
    <input type="text" [(ngModel)]="format">
    <input type="text" ngModel>
    
    <h1>{{value | double}}</h1>
    {{getValue()}}
  `,
})
export class UitkitDemo8PipesComponent implements OnInit {
  filterGenderValue: 'M' | 'F' | 'all' = 'all'
  filterNameValue = ''
  filterASCDESC: 'ASC' | 'DESC' = 'ASC'
  value = 2.10313013103710;
  gb = 10;
  format: 'mb' | 'byte' = 'byte'
  users: User[] = [
    { id: 1, name: 'Michele', age: 30, gender: 'M' },
    { id: 3, name: 'Silvia', age: 20, gender: 'F' },
    { id: 2, name: 'Mich', age: 25, gender: 'F' }
  ];

  ngOnInit(): void {
    setTimeout(() => {
      this.value = 5
    }, 2000)
  }

  getValue() {
    return this.value * 2
  }


}
