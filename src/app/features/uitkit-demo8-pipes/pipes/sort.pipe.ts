import { Pipe, PipeTransform } from '@angular/core';
import { User } from '../uitkit-demo8-pipes.component';

@Pipe({
  name: 'sort',
})
export class SortPipe implements PipeTransform {

  transform(users: User[], order: 'ASC' | 'DESC' = 'ASC'): User[]{
    console.log('sort')
    const result = users.sort((a, b) => a.name.localeCompare(b.name));
    return order === 'ASC' ? result : result.reverse();
  }


}
