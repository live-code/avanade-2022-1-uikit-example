import { Pipe, PipeTransform } from '@angular/core';
import { User } from '../uitkit-demo8-pipes.component';

@Pipe({
  name: 'filterByGender'
})
export class FilterByGenderPipe implements PipeTransform {

  transform(users: User[], gender: 'M' | 'F' | 'all'): User[] {
    console.log(users)
    if (gender === 'all')
      return users;
    return users.filter(u => u.gender === gender)
  }

}
