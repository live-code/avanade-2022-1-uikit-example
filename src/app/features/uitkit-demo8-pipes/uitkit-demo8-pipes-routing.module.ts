import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UitkitDemo8PipesComponent } from './uitkit-demo8-pipes.component';

const routes: Routes = [{ path: '', component: UitkitDemo8PipesComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UitkitDemo8PipesRoutingModule { }
