import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UikitDemo3RoutingModule } from './uikit-demo3-routing.module';
import { UikitDemo3Component } from './uikit-demo3.component';
import { SharedModule } from '../../shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    UikitDemo3Component
  ],
  imports: [
    CommonModule,
    SharedModule,
    ReactiveFormsModule,
    UikitDemo3RoutingModule
  ]
})
export class UikitDemo3Module { }
