import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { debounceTime, map } from 'rxjs/operators';

@Component({
  selector: 'ava-uikit-demo3',
  template: `
    <input type="text" placeholder="City" [formControl]="input">
    <ava-weather 
      [city]="value"
      [color]="myColor"
      [unit]="myUnit"
    ></ava-weather>
    
    <ava-weather [city]="value"></ava-weather>
    
    <button (click)="myUnit = 'metric'">metric</button>
    <button (click)="myUnit = 'imperial'">imperial</button>
    <button (click)="myColor = 'purple'">purple</button>
    <button (click)="myColor = 'cyan'">cyan</button>
  `,
  styles: [
  ]
})
export class UikitDemo3Component implements OnInit {
  value: string | undefined;
  myColor = 'red';
  myUnit: 'metric' | 'imperial' = 'imperial';
  input = new FormControl();

  constructor() {
    this.input.valueChanges
      .pipe(
        map(text => text.toLowerCase()),
        debounceTime(1000)
      )
      .subscribe(text => {
        this.value = text;
      })

  }

  ngOnInit(): void {
  }

}
