import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UikitDemo7Component } from './uikit-demo7.component';

const routes: Routes = [{ path: '', component: UikitDemo7Component }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UikitDemo7RoutingModule { }

