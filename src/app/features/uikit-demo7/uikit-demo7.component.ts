import {
  Component,
  ComponentFactoryResolver, ComponentRef,
  ElementRef,
  OnInit,
  TemplateRef,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import { PadDirective } from '../../shared/directives/pad.directive';
import { CardComponent } from '../../shared/components/card.component';
import { ComponentConfig } from '../../shared/directives/loader.directive';

@Component({
  selector: 'ava-uikit-demo7',
  template: `
    <div
      *ngFor="let component of components"
      [avaLoader]="component"
    >
    </div>
    
    <hr>
    
    <ng-template #tpl>
      yuppi!!!! sono un template dinamico
    </ng-template>
    
    <p avaPad [value]="4" #p="avaPad">
      uikit-demo7 works!
    </p>
    
    <div *avaIfRoleIs="'admin'">solo logged</div>
  `,
})
export class UikitDemo7Component {
  components: ComponentConfig[] = [
    // TODO: sistemare componente weather (ispirarsi a LeafLet component)
    {
      type: 'weather',
      data: {
        city: 'Trieste'
      }
    },
    {
      type: 'leaflet',
      data: {
        coords: [42,13],
        zoom: 10
      }
    },
    {
      type: 'card',
      data: {
        title: 'Profile',
        isOpened: true
      }
    },
    {
      type: 'hello',
      data: {
        name: 'pippo',
        color: 'blue'
      }
    },
  ]

  // example of exportAs
  @ViewChild('p') el!: PadDirective

  @ViewChild('tpl') tpl!: TemplateRef<any>;

  constructor(  private view: ViewContainerRef ) {}

  ngAfterViewInit() {
    setTimeout(() => {
      // show template programmatically
      this.view.createEmbeddedView(this.tpl);

      // show component programmatically

    }, 2000);

    // console.log(this.tpl)
    // console.log(this.el.value); // example of exportAs
  }
}
