import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UikitDemo7RoutingModule } from './uikit-demo7-routing.module';
import { UikitDemo7Component } from './uikit-demo7.component';
import { SharedModule } from '../../shared/shared.module';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    UikitDemo7Component
  ],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    UikitDemo7RoutingModule
  ]
})
export class UikitDemo7Module { }
