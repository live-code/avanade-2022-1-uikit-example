import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../core/auth.service';

@Component({
  selector: 'ava-login',
  template: `
    <p>
      login works! {{authService.data | json}}
    </p>
    <button (click)="authService.login()" 
            mat-flat-button color="primary">Fake Login</button>

  `,
  styles: [
  ]
})
export class LoginComponent implements OnInit {

  constructor(public authService: AuthService) { }

  ngOnInit(): void {
  }

}
