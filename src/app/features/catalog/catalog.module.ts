import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CatalogRoutingModule } from './catalog-routing.module';
import { CatalogComponent } from './catalog.component';
import { CatalogFormComponent } from './components/catalog-form.component';
import { CatalogListComponent } from './components/catalog-list.component';
import { SharedModule } from '../../shared/shared.module';
import { FormsModule } from '@angular/forms';
import { CatalogListItemComponent } from './components/catalog-list-item.component';


@NgModule({
  declarations: [
    CatalogComponent,
    CatalogFormComponent,
    CatalogListComponent,
    CatalogListItemComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    CatalogRoutingModule,
    FormsModule
  ]
})
export class CatalogModule { }
