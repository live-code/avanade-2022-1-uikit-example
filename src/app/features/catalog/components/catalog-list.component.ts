import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { User } from '../catalog.component';

@Component({
  selector: 'ava-catalog-list',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <li *ngFor="let user of users">
      {{user.name}}
      <button (click)="delete.emit(user.id)">x</button>
      <ava-catalog-list-item [username]="username"></ava-catalog-list-item>
    </li>
    
    {{render()}}
    
    <button *avaIfRoleIs="'admin'">Admin only</button>
    
  `,
})
export class CatalogListComponent {
  @Input() users: User[] = [];
  @Input() username!: string
  @Output() delete = new EventEmitter<number>()

  render() {
    console.log('render list')
  }
}
