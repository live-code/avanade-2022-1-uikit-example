import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { AuthService } from '../../../core/auth.service';

@Component({
  selector: 'ava-catalog-list-item',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <p>
      catalog-list-item works! {{username}}
    </p>
  `,
  styles: [
  ]
})
export class CatalogListItemComponent  {
  @Input() username!: string
}
