import { ChangeDetectionStrategy, Component, EventEmitter, OnInit, Output } from '@angular/core';
import { User } from '../catalog.component';

@Component({
  selector: 'ava-catalog-form',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    
    <form #f="ngForm" (submit)="save.emit(f.value)"> 
      <input type="text" ngModel name="name" required #input="ngModel">
      
      {{f.value.name?.length > 3 ? 'long' : 'short'}}
    </form>
    
    {{render()}}
  `,
  styles: [
  ]
})
export class CatalogFormComponent {
  @Output() save = new EventEmitter<User>()

  render() {
    console.log('form render')
  }
}
