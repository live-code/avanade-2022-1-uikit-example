import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { AuthService } from '../../core/auth.service';

@Component({
  selector: 'ava-catalog',
  template: `
    
    <ava-catalog-form
      (save)="addHandler($event)"
    ></ava-catalog-form>
    {{users.length}} users
    <ava-catalog-list
      [users]="users"
      [username]="authService.data?.name"
      (delete)="deleteHandler($event)"
    ></ava-catalog-list>
  `,
  styles: [
  ]
})
export class CatalogComponent {
  users: User[] = [
    { id: 1, name: 'A'},
    { id: 2, name: 'B'},
    { id: 3, name: 'C'},
  ]

  constructor(public authService: AuthService) {

    setTimeout(() => {
      this.authService.data = {
        name: 'pluto'
      }
      this.deleteHandler(2);
    }, 2000)
  }

  deleteHandler(id: number): void {
    // const index = this.users.findIndex(u => u.id === id)
    // this.users.splice(index, 1)
    this.users = this.users.filter(u => u.id !== id)
  }

  addHandler(user: User) {
    // this.users.push(user)
    // this.users = this.users.concat([user]);
    this.users = [...this.users, user]
  }
}

export interface User {
  id: number;
  name: string;
}
