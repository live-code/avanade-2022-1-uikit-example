import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UikitDemo6Component } from './uikit-demo6.component';

const routes: Routes = [{ path: '', component: UikitDemo6Component }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UikitDemo6RoutingModule { }
