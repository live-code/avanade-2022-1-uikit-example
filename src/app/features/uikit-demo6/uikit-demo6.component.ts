import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ava-uikit-demo6',
  template: `
    <button (click)="doSomething()">+</button>
    
    <button avaUrl="http://www.google.com">Google</button>
    <button avaUrl="http://www.adobe.com">adobe</button>
    <button avaUrl>????</button>
    
  
    <div (click)="func2()" style="background-color: red; padding: 10px">
      <div (click)="func1()" avaStopPropagation style="background-color: orange; padding: 10px">CLICK ME</div>
    </div>
    <hr>
    <h3 avaBorder="solid">Directive Example</h3>
    <h3 avaBorder="solid" color="cyan">Directive Example</h3>
    <h3 avaBorder="dashed" color="blue">Directive Example</h3>
  
    <div
      [avaMargin]="value"
      avaPad [value]="value"
      avaHighlight="dark"
    >Generic Example</div>
   
    <div>ciao a <span avaHighlight="light">tutti</span> o 
      a <span avaHighlight="dark">pippo</span></div>
    <hr>
    
    <div #pippo></div>
  `,
})
export class UikitDemo6Component implements OnInit {
  value = 2;
  col = 'pink';
  constructor() { }

  ngOnInit(): void {
  }

  doSomething() {
    this.value++;
    this.col = 'green'
  }

  func1() {
    alert('func 1')
  }
  func2() {
    alert('func 2')
  }
}
