import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UikitDemo6RoutingModule } from './uikit-demo6-routing.module';
import { UikitDemo6Component } from './uikit-demo6.component';
import { SharedModule } from '../../shared/shared.module';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    UikitDemo6Component
  ],
  imports: [
    CommonModule,
    UikitDemo6RoutingModule,
    SharedModule,
    FormsModule
  ]
})
export class UikitDemo6Module { }
