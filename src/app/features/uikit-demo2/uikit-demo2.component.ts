import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'ava-uikit-demo2',
  template: `
    <mat-drawer-container class="example-container" autosize>
      <mat-drawer #drawer class="example-sidenav" mode="over">
        <mat-list>
          <mat-list-item> Pepper </mat-list-item>
          <mat-divider></mat-divider>
          <mat-list-item> Salt </mat-list-item>
          <mat-divider></mat-divider>
          <mat-list-item> Paprika </mat-list-item>
        </mat-list>
      </mat-drawer>

      <div style="display: flex; justify-content: end; margin: 20px">
        <button type="button" mat-button (click)="drawer.toggle()">
          Open
        </button>
      </div>
      
      <div style="max-width: 600px; margin: 0 auto">
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus alias aperiam beatae corporis ea est expedita minima nobis nostrum quo, quos rem similique sit veritatis voluptas. Culpa fugiat ipsa saepe!
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus alias aperiam beatae corporis ea est expedita minima nobis nostrum quo, quos rem similique sit veritatis voluptas. Culpa fugiat ipsa saepe!
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus alias aperiam beatae corporis ea est expedita minima nobis nostrum quo, quos rem similique sit veritatis voluptas. Culpa fugiat ipsa saepe!
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus alias aperiam beatae corporis ea est expedita minima nobis nostrum quo, quos rem similique sit veritatis voluptas. Culpa fugiat ipsa saepe!
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus alias aperiam beatae corporis ea est expedita minima nobis nostrum quo, quos rem similique sit veritatis voluptas. Culpa fugiat ipsa saepe!
        <mat-tab-group>
          <mat-tab
            [label]="user.name"
            *ngFor="let user of users"
          >
            <h2>{{user.address.city}}</h2>
            <p>
              {{user.username}}
            </p>
  
          </mat-tab>
        </mat-tab-group>

        <form class="example-form">
          <mat-form-field class="example-full-width" appearance="fill">
            <mat-label>Email</mat-label>
            <input type="email" matInput [formControl]="emailFormControl" placeholder="Ex. pat@example.com">
            <mat-error *ngIf="emailFormControl.hasError('email') && !emailFormControl.hasError('required')">
              Please enter a valid email address
            </mat-error>
            <mat-error *ngIf="emailFormControl.hasError('required')">
              Email is <strong>required</strong>
            </mat-error>
          </mat-form-field>
          
          <button [disabled]="emailFormControl.invalid">save</button>
        </form>
      </div>

    </mat-drawer-container>
  `,
  styles: [`
    .example-container {
      width: 100vw;
      height: 100vh;
      margin: 0 auto;
    }

    .example-sidenav-content {
      display: flex;
      height: 100%;
      align-items: center;
      justify-content: center;
    }

    .example-sidenav {
      padding: 20px;
    }
  `]
})
export class UikitDemo2Component implements OnInit {
  users: any[] = [];
  showFiller = false;
  emailFormControl = new FormControl('', [Validators.required, Validators.email]);

  constructor(private http: HttpClient) {
    http.get<any[]>('https://jsonplaceholder.typicode.com/users')
      .subscribe(res => {
        this.users = res;
      })
  }

  ngOnInit(): void {
  }

}
