import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UikitDemo4RoutingModule } from './uikit-demo4-routing.module';
import { UikitDemo4Component } from './uikit-demo4.component';
import { SharedModule } from '../../shared/shared.module';


@NgModule({
  declarations: [
    UikitDemo4Component
  ],
  imports: [
    CommonModule,
    SharedModule,
    UikitDemo4RoutingModule
  ]
})
export class UikitDemo4Module { }
