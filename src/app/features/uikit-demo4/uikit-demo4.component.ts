import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ava-uikit-demo4',
  template: `
    
    <ava-leaflet 
      [coords]="coords"
      [zoom]="value"
    ></ava-leaflet>
    <hr>
    <button (click)="coords = [44, 12]">Location 1 </button>
    <button (click)="coords = [45, 13]">Location 2 </button>
    <button (click)="value = value + 1">+</button>
    <button (click)="value = value - 1">-</button>
  `,
  styles: [
  ]
})
export class UikitDemo4Component implements OnInit {
  coords: [number, number] = [43, 13];
  value = 10;

  constructor() { }

  ngOnInit(): void {
  }

}
