import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UikitDemo4Component } from './uikit-demo4.component';

const routes: Routes = [{ path: '', component: UikitDemo4Component }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UikitDemo4RoutingModule { }
