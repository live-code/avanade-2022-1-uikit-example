import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: 'login', loadChildren: () => import('./features/login/login.module').then(m => m.LoginModule) },
  { path: 'catalog', loadChildren: () => import('./features/catalog/catalog.module').then(m => m.CatalogModule) },
  { path: 'uikit-demo1', loadChildren: () => import('./features/uikit-demo1/uikit-demo1.module').then(m => m.UikitDemo1Module) },
  { path: 'uikit-demo2', loadChildren: () => import('./features/uikit-demo2/uikit-demo2.module').then(m => m.UikitDemo2Module) },
  { path: 'uikit-demo3', loadChildren: () => import('./features/uikit-demo3/uikit-demo3.module').then(m => m.UikitDemo3Module) },
  { path: 'uikit-demo4', loadChildren: () => import('./features/uikit-demo4/uikit-demo4.module').then(m => m.UikitDemo4Module) },
  { path: 'uikit-demo5', loadChildren: () => import('./features/uikit-demo5/uikit-demo5.module').then(m => m.UikitDemo5Module) },
  { path: 'uikit-demo6', loadChildren: () => import('./features/uikit-demo6/uikit-demo6.module').then(m => m.UikitDemo6Module) },
  { path: 'uikit-demo7', loadChildren: () => import('./features/uikit-demo7/uikit-demo7.module').then(m => m.UikitDemo7Module) },
  { path: 'uikit8pipes', loadChildren: () => import('./features/uitkit-demo8-pipes/uitkit-demo8-pipes.module').then(m => m.UitkitDemo8PipesModule) }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
