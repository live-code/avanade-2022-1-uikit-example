import { Pipe, PipeTransform } from '@angular/core';
import { LanguageService } from './language.service';

@Pipe({
  name: 'translate',
  pure: false
})
export class TranslatePipe implements PipeTransform {

  transform(key: string): string | null {
    return this.language.getValue(key)
  }

  constructor(private language: LanguageService) {
  }
}
