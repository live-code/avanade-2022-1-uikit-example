import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'mapquest'
})
export class MapquestPipe implements PipeTransform {

  transform(city: string, w: number = 200, h: number = 200): string {
    return `https://www.mapquestapi.com/staticmap/v5/map?key=Go3ZWai1i4nd2o7kBuAUs4y7pnpjXdZn&center=${city}&size=${w},${h}`;
  }

}
