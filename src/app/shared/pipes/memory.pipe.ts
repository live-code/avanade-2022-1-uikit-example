import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'memory'
})
export class MemoryPipe implements PipeTransform {

  transform(
    value: number,
    formatTo: 'mb' | 'byte',
    suffix: string = ''
  ): string {
    switch (formatTo) {
      case 'mb': return value * 1000 + ' ' + suffix;
      case 'byte': return value * 1000000 + ' ' + suffix
      default: return value.toString();
    }

  }

}
