import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatTabsModule } from '@angular/material/tabs';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatDivider, MatDividerModule } from '@angular/material/divider';
import { MatInputModule } from '@angular/material/input';
import { MatDialogModule } from '@angular/material/dialog';
import { WebsiteValidatorDirective } from './forms/website-validator.directive';
import { WeatherComponent } from './components/weather.component';
import { LeafletComponent } from './components/leaflet.component';
import { CardComponent } from './components/card.component';
import { AccordionComponent } from './components/accordion.component';
import { GridRowComponent } from './components/grid-row.component';
import { GridColComponent } from './components/grid-col.component';
import { PadDirective } from './directives/pad.directive';
import { MarginDirective } from './directives/margin.directive';
import { HighlightDirective } from './directives/highlight.directive';
import { BorderDirective } from './directives/border.directive';
import { IfLoggedDirective } from './directives/if-logged.directive';
import { MyRouterLinkActiveDirective } from './directives/my-router-link-active.directive';
import { UrlDirective } from './directives/url.directive';
import { StopPropagationDirective } from './directives/stop-propagation.directive';
import { IfSigninDirective } from './directives/if-signin.directive';
import { IfRoleIsDirective } from './directives/if-role-is.directive';
import { LoaderDirective } from './directives/loader.directive';
import { HelloComponent } from './components/hello.component';
import { DoublePipe } from './pipes/double.pipe';
import { MemoryPipe } from './pipes/memory.pipe';
import { MapquestPipe } from './pipes/mapquest.pipe';
import { TimesagoPipe } from './pipes/timesago.pipe';
import { TranslatePipe } from './pipes/translate.pipe';


@NgModule({
  declarations: [
    WebsiteValidatorDirective,
    WeatherComponent,
    LeafletComponent,
    CardComponent,
    AccordionComponent,
    GridRowComponent,
    GridColComponent,
    PadDirective,
    MarginDirective,
    HighlightDirective,
    BorderDirective,
    IfLoggedDirective,
    MyRouterLinkActiveDirective,
    UrlDirective,
    StopPropagationDirective,
    IfSigninDirective,
    IfRoleIsDirective,
    LoaderDirective,
    HelloComponent,
    DoublePipe,
    MemoryPipe,
    MapquestPipe,
    TimesagoPipe,
    TranslatePipe
  ],
  imports: [
    CommonModule
  ],
  exports: [
    GridRowComponent,
    GridColComponent,
    AccordionComponent,
    CardComponent,
    WeatherComponent,
    MatIconModule,
    MatButtonModule,
    MatListModule,
    MatDividerModule,
    MatMenuModule,
    MatTabsModule,
    MatSidenavModule,
    MatInputModule,
    MatDialogModule,
    WebsiteValidatorDirective,
    LeafletComponent,
    CardComponent,
    PadDirective,
    MarginDirective,
    HighlightDirective,
    BorderDirective,
    IfLoggedDirective,
    MyRouterLinkActiveDirective,
    UrlDirective,
    StopPropagationDirective,
    IfSigninDirective,
    IfRoleIsDirective,
    LoaderDirective,
    HelloComponent,
    DoublePipe,
    MemoryPipe,
    MapquestPipe,
    TimesagoPipe,
    TranslatePipe
  ]
})
export class SharedModule { }
