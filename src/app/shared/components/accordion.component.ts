import { Component, ContentChildren, OnInit, QueryList, ViewChildren } from '@angular/core';
import { CardComponent } from './card.component';

@Component({
  selector: 'ava-accordion',
  template: `
    <div style="border: 1px solid blue; padding: 10px">
      <ng-content></ng-content>
    </div>
  `,
})
export class AccordionComponent  {
  @ContentChildren(CardComponent) cards!: QueryList<CardComponent>;

  ngAfterContentInit() {
    console.log(this.cards)
    const cards = this.cards.toArray();
    for (let i = 0; i < cards.length; i++) {
      cards[i].isOpened = false;
      cards[i].headerClick.subscribe(() => {
        this.openPanel(cards[i])
      })
    }
    cards[0].isOpened = true;
  }

  openPanel(panel: CardComponent) {
    const cards = this.cards.toArray();
    for (let i = 0; i < cards.length; i++) {
      cards[i].isOpened = false;
    }

    panel.isOpened = true;
  }

  openAll(val: boolean) {
    const cards = this.cards.toArray();
    for (let i = 0; i < cards.length; i++) {
      cards[i].isOpened = val;
    }
  }
}
