import { Component, HostBinding, Input, OnInit, Optional } from '@angular/core';
import { GridRowComponent } from './grid-row.component';
import { AppComponent } from '../../app.component';

@Component({
  selector: 'ava-grid-col',
  template: `
    <ng-content></ng-content>
  `,
})
export class GridColComponent {
  @HostBinding() get class() {
    return 'col-' + (this.parent ? this.parent.mq : 'sm')
  }

  constructor(
    @Optional() private parent: GridRowComponent,
    app: AppComponent
  ) {
    app.title = 'caioaioa'
    if (!parent) {
      throw new Error('you have use col inside a row component')
    }

  }
}
