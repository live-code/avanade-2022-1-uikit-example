import {
  AfterViewInit,
  Component,
  ElementRef,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import * as L from 'leaflet';

@Component({
  selector: 'ava-leaflet',
  template: `
    <div #map class="map"></div>
  `,
  styles: [`
    .map { height: 200px }
  `]
})
export class LeafletComponent implements OnChanges {
  @Input() coords: [number, number] | null = null;
  @Input() zoom: number = 5;
  @ViewChild('map', { static: true}) mapReference!: ElementRef<HTMLDivElement>;
  map!: L.Map;
  isAlreadInitialized = false;

  init() {
    if (this.coords) {
      this.map = L.map(this.mapReference.nativeElement)
        .setView(this.coords, this.zoom);

      L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
      }).addTo(this.map);
    }
  }

  ngOnInit() {
    if (this.isAlreadInitialized) return;
    console.log('init')
    this.render(this.coords, this.zoom)
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.isAlreadInitialized = true;
    console.log('ngOnChanges')
    this.render(
      changes['coords']?.currentValue,
      changes['zoom']?.currentValue
    )
  }

  render(coords: [number, number] | null, zoom: number) {
    console.log('render')
    if (!this.map) {
      this.init();
    }
    if (coords) {
      this.map.setView(coords)
    }

    if (zoom) {
      this.map.setZoom(this.zoom)
    }
  }
}
