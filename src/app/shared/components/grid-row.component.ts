import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'ava-grid-row',
  template: `
    <div class="row">
      <ng-content></ng-content>
    </div>
  `,
  styles: [
  ]
})
export class GridRowComponent implements OnInit {
  @Input() mq: 'sm' | 'md' | 'lg' | null = null;
  constructor() { }

  ngOnInit(): void {
  }

}
