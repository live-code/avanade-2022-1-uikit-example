import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'ava-hello',
  template: `
    <h1 [style.color]="color">
      hello {{name}}
    </h1>
  `,
  styles: [
  ]
})
export class HelloComponent implements OnInit {
  @Input() name = ''
  @Input() color = ''
  constructor() { }

  ngOnInit(): void {
  }

}
