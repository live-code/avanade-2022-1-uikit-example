import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'ava-card',
  template: `
    <div class="card">
      <div class="card-header"
           (click)="headerClick.emit()"
      >{{title}}</div>
      <div class="card-body" 
           *ngIf="isOpened">
        <ng-content></ng-content>
      </div>
    </div>
  `,
  styles: [`
    .card {
      margin: 10px;
    }
    .card-header {
      background-color: #222;
      padding: 10px;
      color: white;
    }
    
    .card-body {
      border: 1px solid black;
      padding: 10px;
    }
  `]
})
export class CardComponent  {
  @Input() title = '';
  @Input() isOpened = true;
  @Output() headerClick = new EventEmitter()

  getValue() {
    return this.isOpened;
  }
}
