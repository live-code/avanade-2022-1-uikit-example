import { ChangeDetectionStrategy, Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Meteo } from '../../model/meteo';

@Component({
  selector: 'ava-weather',
  template: `
      <h1 [style.color]="color">Weather: {{city}}</h1>
      <pre *ngIf="meteo">{{meteo.main?.temp}}°</pre>
  `,
  styles: [
  ]
})
export class WeatherComponent implements OnChanges {
  @Input() city: string | undefined;
  @Input() color: string = 'blue';
  @Input() unit: 'metric' | 'imperial' = 'metric'

  meteo: Meteo | null = null;

  constructor(private http: HttpClient) { }

  ngOnChanges(changes: SimpleChanges): void {
    if (
      (changes['city'] && changes['city'].currentValue)
    ) {
      this.fetchData()
    }
    if(changes['unit']) {
      this.fetchData()
    }
  }

  fetchData() {
    if (this.city) {
      this.http.get<Meteo>(`https://api.openweathermap.org/data/2.5/weather?q=${this.city}&units=${this.unit}&APPID=eb03b1f5e5afb5f4a4edb40c1ef2f534`)
        .subscribe(res => {
          this.meteo = res;
        })
    }
  }

}
