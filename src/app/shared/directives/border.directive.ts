import { Directive, ElementRef, Input, Renderer2, SimpleChanges } from '@angular/core';
import { AutobahnService } from '../../core/services/autobahn.service';

// <div avaBorder="dashed" color="blue"

@Directive({
  selector: '[avaBorder]'
})
export class BorderDirective {
  @Input() avaBorder = 'none';
  @Input() color = 'black';

  ngOnChanges(changes: SimpleChanges) {
    this.renderer.setStyle(
      this.el.nativeElement,
      'border',
      `1px ${this.avaBorder} ${this.color}`
    )
  }

  constructor(
    private el: ElementRef,
    private renderer: Renderer2,
  ) {
  }

  ngOnDestroy() {
    console.log('destroy border directive')
  }
}
