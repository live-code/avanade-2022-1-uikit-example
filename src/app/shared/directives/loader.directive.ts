import {
  ChangeDetectorRef,
  ComponentFactoryResolver,
  ComponentRef,
  Directive,
  HostBinding,
  Input,
  Type,
  ViewContainerRef
} from '@angular/core';
import { CardComponent } from '../components/card.component';
import { LeafletComponent } from '../components/leaflet.component';
import { HelloComponent } from '../components/hello.component';
import { WeatherComponent } from '../components/weather.component';
import { ChangeDetection } from '@angular/cli/lib/config/workspace-schema';

const COMPONENTS: { [key: string]: Type<any> } = {
  'card': CardComponent,
  'hello': HelloComponent,
  'leaflet': LeafletComponent,
  'weather': WeatherComponent, // sistemare -> deve gestire onInit come per Leaflet
}

export interface ComponentConfig {
  type: string,
  data: any;
}

@Directive({
  selector: '[avaLoader]'
})
export class LoaderDirective {
  componentRef!: ComponentRef<any>;


  @Input() set avaLoader(config: ComponentConfig) {
    this.view.clear();

    try {
      const resolver = this.resolver.resolveComponentFactory(COMPONENTS[config.type])
      this.componentRef = this.view.createComponent(resolver);
      for (let key in config.data) {
        this.componentRef.instance[key] = config.data[key]
      }
    } catch(err) {}
  }

  constructor(
    private view: ViewContainerRef,
    private resolver: ComponentFactoryResolver,
  ) {
  }

  ngOnDestroy() {
    if (this.componentRef) {
      this.componentRef.destroy()
    }
  }
}
