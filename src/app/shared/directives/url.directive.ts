import { Directive, HostListener, Input } from '@angular/core';

// <button avaUrl="http://...." >

@Directive({
  selector: '[avaUrl]'
})
export class UrlDirective {
  @Input() avaUrl!: string

  @HostListener('click')
  openUrl() {
    if (this.avaUrl) {
      window.open(this.avaUrl)
    } else {
      throw new Error('you should specify the url')
    }
  }

}
