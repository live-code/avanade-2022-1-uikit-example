import { Directive, HostBinding, HostListener } from '@angular/core';

/*
*  <div (click)="clickHandler($event)">
*    <div avaStopPropagation (click)="">
*/
@Directive({
  selector: '[avaStopPropagation]'
})
export class StopPropagationDirective {

  @HostListener('click', ['$event'])
  doSomething(event: MouseEvent) {
    event.stopPropagation();
  }

}
