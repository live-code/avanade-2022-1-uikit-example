import { Directive, ElementRef, Input } from '@angular/core';

@Directive({
  selector: '[avaMargin]'
})
export class MarginDirective {
  @Input() set avaMargin(val: number) {
    this.el.nativeElement.style.margin = `${val*10}px`
  }

  constructor(private el: ElementRef) {
  }
}
