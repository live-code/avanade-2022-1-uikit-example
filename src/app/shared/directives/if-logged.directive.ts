import { Directive, ElementRef, HostBinding, Renderer2 } from '@angular/core';
import { AuthService } from '../../core/auth.service';

// <div avaIfLogged>

@Directive({
  selector: '[avaIfLogged]'
})
export class IfLoggedDirective {
  // SOLUTION 1: with HostBinding
/*  @HostBinding('style.display') get display() {
    return !this.authService.isLogged() ? 'none' : null
  }*/

  constructor(
    private authService: AuthService,
    private el: ElementRef<HTMLElement>,
    private renderer: Renderer2
  ) {

    authService.data$
      .subscribe(data => {
        if (data) {
          // if logged --> SHOW
          renderer.removeAttribute(this.el.nativeElement, 'hidden')
          // renderer.removeStyle(this.el.nativeElement, 'display')
        } else {
          // if logout --> HIDE
          renderer.setAttribute(this.el.nativeElement, 'hidden', '')
          // renderer.setStyle(this.el.nativeElement, 'display', 'none')

        }
      })
  }

}
