import { Directive, HostBinding, Input } from '@angular/core';

/*<div avaPad [value]="..."*/
/*<div avaPad [value]="5"*/

@Directive({
  selector: '[avaPad]',
  exportAs: 'avaPad'
})
export class PadDirective {

  @Input() value: number = 0;
  @Input() color: string | undefined;

  /*@HostBinding() get className() {
    console.log('render', this.value)
    return 'p-' +  this.value
  }*/

  @HostBinding('style.color') get getColor() {
    return this.color;
  }

  @HostBinding('style.padding') get getMargin() {
    if (this.value > 5) return '50px';
    return `${10 * this.value}px`
  }
}

