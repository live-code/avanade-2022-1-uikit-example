import { Directive, ElementRef, HostBinding, Renderer2, TemplateRef, ViewContainerRef } from '@angular/core';
import { AuthService } from '../../core/auth.service';


// <div *avaIfSignin>

@Directive({
  selector: '[avaIfSignin]'
})
export class IfSigninDirective {

  constructor(
    private authService: AuthService,
    private el: ElementRef<HTMLElement>,
    private template: TemplateRef<any>,
    private view: ViewContainerRef
  ) {
    authService.data$
      .subscribe(data => {
        if (data) {
          // if logged --> SHOW
          view.createEmbeddedView(template)
        } else {
          // if logout --> HIDE
          view.clear();
        }
      })
  }

}
