import { Directive, ElementRef, Input, TemplateRef, ViewContainerRef } from '@angular/core';
import { AuthService } from '../../core/auth.service';

// <button *avaIfRoleIs="admin"

@Directive({
  selector: '[avaIfRoleIs]'
})
export class IfRoleIsDirective {
  @Input() avaIfRoleIs!: string

  constructor(
    private authService: AuthService,
    private el: ElementRef<HTMLElement>,
    private template: TemplateRef<any>,
    private view: ViewContainerRef
  ) {
  }

  ngOnInit() {
    this.authService.data$
      .subscribe(data => {
        if (data?.role === this.avaIfRoleIs) {
          // if logged --> SHOW
          this.view.createEmbeddedView(this.template)
        } else {
          // if logout --> HIDE
          this.view.clear();
        }
      })
  }

}
