import { Directive, ElementRef, Input, Renderer2 } from '@angular/core';
import { divIcon } from 'leaflet';


const config = {
  light: 'yellow',
  dark: 'orange'
}

// <div avaHighlight="dark"

@Directive({
  selector: '[avaHighlight]'
})
export class HighlightDirective {

  @Input() set avaHighlight(color: 'dark' | 'light') {
    // this.el.nativeElement.style.backgroundColor = config[color];
    this.renderer.setStyle(
      this.el.nativeElement,
      'backgroundColor',
      config[color]
    )
  }

  constructor(
    private el: ElementRef<HTMLElement>,
    private renderer: Renderer2
  ) {
  }

}
