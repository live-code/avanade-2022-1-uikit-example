import { Directive, ElementRef, Input, Renderer2 } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';

// <button routerLink="page1" avaMyRouterLinkActive="active"

@Directive({
  selector: '[avaMyRouterLinkActive]'
})
export class MyRouterLinkActiveDirective {
  @Input() avaMyRouterLinkActive!: string;

  constructor(
    private router: Router,
    private el: ElementRef<HTMLElement>,
    private renderer: Renderer2,
  ) {

    const requiredUrl = this.el.nativeElement.getAttribute('routerLink');
    router.events
      .subscribe(ev => {
        if (ev instanceof NavigationEnd) {
          if (requiredUrl && ev.url.includes(requiredUrl)) {
            this.renderer.addClass(this.el.nativeElement, this.avaMyRouterLinkActive)
          } else {
            this.renderer.removeClass(this.el.nativeElement, this.avaMyRouterLinkActive)
          }
        }
      })
  }

}
