import { Directive } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, ValidationErrors, Validator } from '@angular/forms';

const REGEX_URL = /[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)/

@Directive({
  selector: '[avaWebsiteValidator]',
  providers: [
    { provide: NG_VALIDATORS, useClass: WebsiteValidatorDirective, multi: true}
  ]
})
export class WebsiteValidatorDirective implements Validator {

  validate(control: AbstractControl): ValidationErrors | null {
    return control.value?.match(REGEX_URL) ? null : {
      website: true
    }
  }

}
